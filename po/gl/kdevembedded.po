# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
# SPDX-FileCopyrightText: 2024, 2025 Adrián Chaves (Gallaecio)
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-05 12:27+0000\n"
"PO-Revision-Date: 2025-01-26 19:43+0100\n"
"Last-Translator: Adrián Chaves (Gallaecio) <adrian@chaves.io>\n"
"Language-Team: Proxecto Trasno (proxecto@trasno.gal)\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 24.12.1\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Adrian Chaves (Gallaecio)"

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "adrian@chaves.gal"

#: embedded.cpp:60
#, kde-format
msgid "Arduino Setup"
msgstr "Preparación de Arduino"

#: embedded.cpp:61
#, kde-format
msgid "Configure Arduino Toolkit."
msgstr "Configurar o xogo de ferramentas de Arduino."

#: embedded.cpp:62
#, kde-format
msgid "Toolkit manager for Arduino programs."
msgstr "Xestor de xogos de ferramentas para programas de Arduino."

#: firsttimewizard.cpp:99
#, kde-format
msgid "<p>More information at: <a href=\"mailto:%1\">%1</a></p>"
msgstr "<p>Máis información en: <a href=\"mailto:%1\">%1</a></p>"

#: firsttimewizard.cpp:100
#, kde-format
msgid "Embedded plugin is an unofficial project by Patrick J. Pereira."
msgstr ""
"O complemento de integrados é un proxecto non oficial de Patrick J. Pereira."

#: firsttimewizard.cpp:116
#, kde-format
msgid "Arduino %1 for %2"
msgstr "Arduino %1 para %2"

#: firsttimewizard.cpp:213
#, kde-format
msgid "Downloading..."
msgstr "Descargando…"

#: firsttimewizard.cpp:224
#, kde-format
msgid "Downloaded"
msgstr "Descargado"

#: firsttimewizard.cpp:228
#, kde-format
msgid "Download cancelled"
msgstr "Cancelouse a descarga."

#: firsttimewizard.cpp:251
#, kde-format
msgid "Extracting..."
msgstr "Extraendo…"

#: firsttimewizard.cpp:263
#, kde-format
msgid "Extracting... "
msgstr "Extraendo…"

#: firsttimewizard.cpp:275
#, kde-format
msgid "Extracted"
msgstr "Extraeuse."

#: firsttimewizard.cpp:377 firsttimewizard.cpp:387
#, kde-format
msgid "Find Files"
msgstr "Atopar ficheiros"

#: firsttimewizard.cpp:405
#, kde-format
msgid "Downloading... (%1 / %2)"
msgstr "Descargando… (%1 / %2)"

#. i18n: ectx: property (windowTitle), widget (QWizard, FirstTimeWizard)
#: firsttimewizard.ui:14
#, kde-format
msgid "Wizard"
msgstr "Asistente"

#. i18n: ectx: property (title), widget (QWizardPage, wizardPage1)
#: firsttimewizard.ui:21
#, kde-format
msgid "First-time configuration"
msgstr "Configuración inicial."

#. i18n: ectx: property (subTitle), widget (QWizardPage, wizardPage1)
#: firsttimewizard.ui:24
#, kde-format
msgid ""
"The plugin needs some information in order to work correctly.<br />Please "
"fill them in the form below:"
msgstr ""
"O complemento necesita algunha información para funcionar correctamente.<br/"
">Complete os campos do seguinte formulario:"

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: firsttimewizard.ui:33
#, kde-format
msgid "The Arduino location is needed to continue."
msgstr "Necesítase a localización de Arduino para continuar."

#. i18n: ectx: property (text), widget (QRadioButton, existingInstallButton)
#: firsttimewizard.ui:40
#, kde-format
msgid "E&xisting installation"
msgstr "Instalación e&xistente."

#. i18n: ectx: property (text), widget (QLabel, label)
#: firsttimewizard.ui:67
#, kde-format
msgid "Ard&uino path:"
msgstr "Ruta de Ard&uino:"

#. i18n: ectx: property (text), widget (QToolButton, arduinoPathButton)
#. i18n: ectx: property (text), widget (QToolButton, sketchbookPathButton)
#: firsttimewizard.ui:82 firsttimewizard.ui:163
#, kde-format
msgid "..."
msgstr "…"

#. i18n: ectx: property (text), widget (QRadioButton, automaticInstallButton)
#: firsttimewizard.ui:95
#, kde-format
msgid "Auto&matic installation"
msgstr "Instalación auto&mática."

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: firsttimewizard.ui:120
#, kde-format
msgid ""
"<p>This will download and install the software automatically from <a href="
"\"http://arduino.cc/\">the Arduino website</a>.</p>"
msgstr ""
"<p>Isto descargará e instalará os programas automaticamente desde o <a href="
"\"http://arduino.cc/\">sitio web de Arduino</a>.</p>"

#. i18n: ectx: property (text), widget (QLabel, label_6)
#. i18n: ectx: property (text), widget (QLabel, label_11)
#: firsttimewizard.ui:132 firsttimewizard.ui:256
#, kde-format
msgid "<hr />"
msgstr "<hr />"

#. i18n: ectx: property (text), widget (QLabel, label_4)
#: firsttimewizard.ui:139
#, kde-format
msgid ""
"<p>The sketchbook folder is where the plugin will look for existing projects."
"<br/>It will be created if it does not exist already.</p>"
msgstr ""
"<p>O cartafol do caderno de bosquexos é onde o complemento buscará proxectos "
"existentes.<br/>Crearase se aínda non existe.</p>"

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: firsttimewizard.ui:148
#, kde-format
msgid "S&ketchbook path:"
msgstr "Ruta do caderno de bos&quexos:"

#. i18n: ectx: property (title), widget (QWizardPage, wizardPage2)
#: firsttimewizard.ui:175
#, kde-format
msgid "Automatic Installation"
msgstr "Instalación automática"

#. i18n: ectx: property (subTitle), widget (QWizardPage, wizardPage2)
#: firsttimewizard.ui:178
#, kde-format
msgid "The plugin will now download and install the required software."
msgstr ""
"O complemento procederá a descargar e instalar os programas necesarios."

#. i18n: ectx: property (text), widget (QLabel, label_8)
#: firsttimewizard.ui:189
#, kde-format
msgid "Step 1: Download"
msgstr "Paso 1: Descarga"

#. i18n: ectx: property (text), widget (QLabel, label_9)
#: firsttimewizard.ui:265
#, kde-format
msgid "Step 2: Installation"
msgstr "Paso 2: Instalación"

#. i18n: ectx: property (title), widget (QWizardPage, wizardPage3)
#: firsttimewizard.ui:308
#, kde-format
msgid "Configuration successful"
msgstr "Completouse a configuración."

#. i18n: ectx: property (subTitle), widget (QWizardPage, wizardPage3)
#: firsttimewizard.ui:311
#, kde-format
msgid "The plugin is now ready."
msgstr "O complemento está listo."

#. i18n: ectx: property (text), widget (QLabel, label_7)
#: firsttimewizard.ui:320
#, kde-format
msgid ""
"The configuration is complete, you may now start using the IDE.<br />Thank "
"you for using the software. Enjoy!"
msgstr ""
"Completouse a configuración, pode comezar a usar o IDE.<br />Grazas por usar "
"este programa. Páseo ben!"

#. i18n: ectx: Menu (Embedded)
#: kdevembedded.rc:5
#, kde-format
msgctxt "@title:menu"
msgid "Embedded"
msgstr "Integrados"

#: launcher/embeddedlauncher.cpp:234
#, kde-format
msgid "kdev-embedded"
msgstr "kdev-embedded"

#: launcher/embeddedlauncher.cpp:234
#, kde-format
msgid "Please run the first time wizard."
msgstr "Execute o asistente inicial."

#: launcher/embeddedlauncher.cpp:243
#, kde-format
msgid "Please add a microcontroller"
msgstr "Engada un microcontrolador."

#: launcher/embeddedlauncher.cpp:253
#, kde-format
msgid ""
"Please connect or select an interface, for example:\n"
"/dev/ttyUSBx, /dev/ttyACMx, COMx, etc"
msgstr ""
"Conecte ou seleccione unha interface. Por exemplo:\n"
"/dev/ttyUSBx, /dev/ttyACMx, COMx, etc."

#: launcher/embeddedlauncher.cpp:261
#, kde-format
msgid ""
"Please choose or select a baudrate:\n"
"19200, 57600, 115200, etc"
msgstr ""
"Escolla un seleccione unha velocidade en baud:\n"
"19200, 57600, 115200, etc."

#: launcher/embeddedlauncher.cpp:269
#, kde-format
msgid "Variables to programmer:\n"
msgstr "Variábeis para o programador:\n"

#: launcher/embeddedlauncher.cpp:270
#, kde-format
msgid "%avrdudeconf\t- Specify location of configuration file.\n"
msgstr "%avrdudeconf\t- Indique a localización do ficheiro de configuración.\n"

#: launcher/embeddedlauncher.cpp:271
#, kde-format
msgid "%mcu\t- Required. Specify AVR device.\n"
msgstr "%mcu\t- Obrigatorio. Indique un dispositivo AVR.\n"

#: launcher/embeddedlauncher.cpp:272
#, kde-format
msgid "%interface\t- Specify connection port.\n"
msgstr "%interface\t- Indique un porto de conexión.\n"

#: launcher/embeddedlauncher.cpp:273
#, kde-format
msgid "%baud\t- Override RS-232 baud rate.\n"
msgstr "%baud\t- Sobredefinir a velocidade en baud RS-232.\n"

#: launcher/embeddedlauncher.cpp:274
#, kde-format
msgid "%hex\t- Firmware."
msgstr "%hex\t- Firmware."

# well-spelled: microcontroladores
#: launcher/embeddedlauncher.cpp:281
#, kde-format
msgid ""
"%avrdude - Avrdude is a program for downloading code and data to Atmel AVR "
"microcontrollers."
msgstr ""
"%avrdude - Avrdude é un programa para descargar código e datos a "
"microcontroladores AVR de Atmel."

#: launcher/embeddedlauncher.cpp:345
#, kde-format
msgid "Configure Embedded Application"
msgstr "Configurar a aplicación integrada"

#: launcher/embeddedlauncher.cpp:359
#, kde-format
msgid "Upload applications to embedded platforms"
msgstr "Enviar aplicacións a plataformas integradas"

#: launcher/embeddedlauncher.cpp:369 launcher/embeddedlauncher.cpp:430
#, kde-format
msgid "Embedded"
msgstr "Integrado"

#: launcher/embeddedlauncher.cpp:510
#, kde-format
msgid "Embedded Binary"
msgstr "Binario integrado"

#: launcher/embeddedlauncher.cpp:634
#, kde-format
msgid "Could not find interface"
msgstr "Non foi posíbel atopar a interface."

#. i18n: ectx: property (title), widget (QGroupBox, groupBox)
#: launcher/embeddedlauncher.ui:17
#, kde-format
msgid "Firmware"
msgstr "Firmware"

#. i18n: ectx: property (text), widget (QLabel, label)
#: launcher/embeddedlauncher.ui:23
#, kde-format
msgid "Pro&ject Target:"
msgstr "Destino do pro&xecto:"

#. i18n: ectx: property (text), widget (QLabel, label2)
#: launcher/embeddedlauncher.ui:63
#, kde-format
msgid "Exec&utable:"
msgstr "Exec&utábel:"

#. i18n: ectx: property (placeholderText), widget (KUrlRequester, executablePath)
#: launcher/embeddedlauncher.ui:88
#, kde-format
msgid "Enter the executable name or absolute path to an executable file"
msgstr ""
"Insira o nome do executábel ou unha ruta absoluta a un ficheiro executábel."

#. i18n: ectx: property (text), widget (QLabel, freqLabel)
#: launcher/embeddedlauncher.ui:126
#, kde-format
msgid "MCU:"
msgstr "MCU:"

#. i18n: ectx: property (text), widget (QLabel, boardLabel)
#: launcher/embeddedlauncher.ui:136
#, kde-format
msgid "Board:"
msgstr "Placa:"

#. i18n: ectx: property (text), widget (QLabel, interfaceLabel)
#: launcher/embeddedlauncher.ui:146
#, kde-format
msgid "Interface:"
msgstr "Interface:"

#. i18n: ectx: property (text), widget (QLabel, presetsLabel)
#. i18n: ectx: property (text), widget (QLabel, presetsLabel2)
#: launcher/embeddedlauncher.ui:159 launcher/embeddedlauncher.ui:391
#, kde-format
msgid "Presets:"
msgstr "Predefinicións:"

#. i18n: ectx: property (text), item, widget (QComboBox, presetsComboPage1)
#. i18n: ectx: property (text), item, widget (QComboBox, presetsComboPage2)
#: launcher/embeddedlauncher.ui:176 launcher/embeddedlauncher.ui:405
#, kde-format
msgid "Arduino"
msgstr "Arduino"

#. i18n: ectx: property (text), item, widget (QComboBox, presetsComboPage1)
#. i18n: ectx: property (text), item, widget (QComboBox, presetsComboPage2)
#: launcher/embeddedlauncher.ui:181 launcher/embeddedlauncher.ui:410
#, kde-format
msgid "OpenOCD"
msgstr "OpenOCD"

#. i18n: ectx: property (text), item, widget (QComboBox, boardCombo)
#: launcher/embeddedlauncher.ui:196
#, kde-format
msgid "Nano"
msgstr "Nano"

#. i18n: ectx: property (toolTip), widget (KComboBox, interfaceCombo)
#: launcher/embeddedlauncher.ui:210
#, kde-format
msgid "Please, connect an interface"
msgstr "Conecte unha interface."

#. i18n: ectx: property (text), widget (QLabel, interfaceBRLabel)
#: launcher/embeddedlauncher.ui:258
#, kde-format
msgid "            Baud rate:"
msgstr "            Velocidade en baud:"

#. i18n: ectx: property (text), widget (QLabel, workdirLabel)
#. i18n: ectx: property (text), widget (QLabel, workdirLabel2)
#: launcher/embeddedlauncher.ui:265 launcher/embeddedlauncher.ui:456
#, kde-format
msgid "Working Directory:"
msgstr "Cartafol de traballo:"

#. i18n: ectx: property (toolTip), widget (KUrlRequester, workingDirectory)
#. i18n: ectx: property (placeholderText), widget (KUrlRequester, workingDirectory)
#. i18n: ectx: property (toolTip), widget (KUrlRequester, openocdWorkingDirectory)
#. i18n: ectx: property (placeholderText), widget (KUrlRequester, openocdWorkingDirectory)
#: launcher/embeddedlauncher.ui:281 launcher/embeddedlauncher.ui:284
#: launcher/embeddedlauncher.ui:469 launcher/embeddedlauncher.ui:472
#, kde-format
msgid "Select a working directory for the executable"
msgstr "Escolla un cartafol de traballo para o executábel."

#. i18n: ectx: property (text), item, widget (KComboBox, argumentsCombo)
#: launcher/embeddedlauncher.ui:304
#, no-c-format, kde-format
msgid ""
"-C%avrdudeconf -v true -p%mcu -carduino -P%interface -b%baud -D -Uflash:w:"
"%hex:i"
msgstr ""
"-C%avrdudeconf -v true -p%mcu -carduino -P%interface -b%baud -D -Uflash:w:"
"%hex:i"

#. i18n: ectx: property (text), widget (QLabel, argumentsLabel)
#. i18n: ectx: property (text), widget (QLabel, argumentsLabel2)
#: launcher/embeddedlauncher.ui:312 launcher/embeddedlauncher.ui:418
#, kde-format
msgid "Arguments:"
msgstr "Parámetros:"

#. i18n: ectx: property (text), widget (QLabel, commandLabel)
#. i18n: ectx: property (text), widget (QLabel, commandLabel2)
#: launcher/embeddedlauncher.ui:322 launcher/embeddedlauncher.ui:479
#, kde-format
msgid "Command:"
msgstr "Orde:"

#. i18n: ectx: property (toolTip), widget (KComboBox, commandCombo)
#. i18n: ectx: property (toolTip), widget (KComboBox, openocdCommand)
#: launcher/embeddedlauncher.ui:348 launcher/embeddedlauncher.ui:504
#, no-c-format, kde-format
msgid ""
"<p>Defines the command to execute the external terminal emulator. Use the "
"following placeholders:</p>\n"
"<dl>\n"
"<dt><code>%exe</code></dt>\n"
"<dd>The path to the executable selected above.</dd>\n"
"<dt><code>%workdir</code></dt>\n"
"<dd>The path to the working directory selected above.</dd>\n"
"</dl>\n"
"<p>The arguments defined above will get appended to this command.</p>"
msgstr ""
"<p>Define a orde para executar o emulador de terminal externo. Pode usar os "
"símbolos seguintes:</p>\n"
"<dl>\n"
"<dt><code>%exe</code></dt>\n"
"<dd>Ruta ao executábel seleccionado máis arriba.</dd>\n"
"<dt><code>%workdir</code></dt>\n"
"<dd>Ruta ao cartafol de traballo seleccionado máis arriba.</dd>\n"
"</dl>\n"
"<p>Os argumentos definidos máis arriba engadiranse a esta orde.</p>"

#. i18n: ectx: property (text), item, widget (KComboBox, commandCombo)
#: launcher/embeddedlauncher.ui:358
#, no-c-format, kde-format
msgid "%avrdude"
msgstr "%avrdude"

#. i18n: ectx: property (text), item, widget (KComboBox, commandCombo)
#: launcher/embeddedlauncher.ui:363
#, kde-format
msgid "/usr/bin/avrdude"
msgstr "/usr/bin/avrdude"

#. i18n: ectx: property (text), item, widget (KComboBox, openocdArgumentsCombo)
#: launcher/embeddedlauncher.ui:438
#, kde-format
msgid "-f board/board.cfg"
msgstr "-f placa/placa.cfg"

#. i18n: ectx: property (text), item, widget (KComboBox, openocdArgumentsCombo)
#: launcher/embeddedlauncher.ui:443
#, kde-format
msgid "-f interface/interface.cfg -f target/target.cfg"
msgstr "-f interface/interface.cfg -f destino/destino.cfg"

# skip-rule: trasno-erase
#. i18n: ectx: property (text), item, widget (KComboBox, openocdArgumentsCombo)
#: launcher/embeddedlauncher.ui:448
#, no-c-format, kde-format
msgid ""
"-f board/board.cfg -c init -c targets -c \"reset\" -c \"halt\" -c \"flash "
"write_image erase %hex\" -c \"verify_image %hex\" -c \"reset run\" -c "
"shutdown"
msgstr ""
"-f placa/placa.cfg -c init -c targets -c \"reset\" -c \"halt\" -c \"flash "
"write_image erase %hex\" -c \"verify_image %hex\" -c \"reset run\" -c "
"shutdown"

#. i18n: ectx: property (text), item, widget (KComboBox, openocdCommand)
#: launcher/embeddedlauncher.ui:514
#, no-c-format, kde-format
msgid "%openocd"
msgstr "%openocd"

#. i18n: ectx: property (text), item, widget (KComboBox, openocdCommand)
#: launcher/embeddedlauncher.ui:519
#, kde-format
msgid "/usr/bin/openocd"
msgstr "/usr/bin/openocd"

#: launcher/executeplugin.cpp:128
#, kde-format
msgid ""
"There is a quoting error in the arguments for the launch configuration '%1'. "
"Aborting start."
msgstr ""
"Hai un erro de comiñas nos argumentos da configuración de iniciar «%1». "
"Interrómpese o inicio."

#: launcher/executeplugin.cpp:133
#, kde-format
msgid ""
"A shell meta character was included in the arguments for the launch "
"configuration '%1', this is not supported currently. Aborting start."
msgstr ""
"Incluíuse un carácter meta de intérprete de ordes nos argumentos da "
"configuración de iniciar «%1», o que non se admite de momento. Interrómpese "
"o inicio."

#: launcher/launcherjob.cpp:168
#, kde-format
msgid "Job already running"
msgstr "O traballo xa está en execución"

#: launcher/launcherjob.cpp:168
#, kde-format
msgid "'%1' is already being executed. Should we kill the previous instance?"
msgstr "«%1» xa está en execución. Quere matar a instancia anterior?"
